from django.contrib.auth import views as auth_views
from django.urls import path
from accounts.views import create_user


urlpatterns = [
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="registration/login.html"),
        name="login",
    ),
    path(
        "logout/",
        auth_views.LogoutView.as_view(
            template_name="registration/logout.html"
        ),
        name="logout",
    ),
    path(
        "signup/",
        create_user,
        name="signup",
    ),
]
