from django import template
import datetime
from django.utils import timezone

register = template.Library()


def upcoming(task):
    print("task: ", task)
    print("datetime: ", timezone.now())
    timeleft = timezone.now() - task

    return timeleft


register.filter(upcoming)
