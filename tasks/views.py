from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy, reverse
from projects.models import Project
from tasks.models import Task
import datetime

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        projectlist = []

        print(datetime.datetime.now())
        # print(Task.due_date)
        # delta = d1 - d0
        # print(delta)
        # Create a new empty list and assign it to a variable

        for proj in self.request.user.projects.all():
            projectlist.append(proj.name)
        context["projects_list"] = projectlist
        context["projects"] = Project.objects.filter(members=self.request.user)
        # Put that list into the context
        return context

    # def get_queryset(self):
    #     print(Task.project.all)
    #     return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(UpdateView):
    model = Task
    template_name = "tasks/list.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
